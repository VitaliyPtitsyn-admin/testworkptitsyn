package test.testwork.main;

import android.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import test.testwork.R;
import test.testwork.main.ui.FragmentConnection;
import test.testwork.main.ui.Fragment_request;
import test.testwork.main.ui.Fragment_table;


public class ActivityMain extends ActionBarActivity {

    private FragmentTransaction fTrans;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializationFragments(savedInstanceState);
    }

    private void initializationFragments(Bundle savedInstanceState) {
        if(savedInstanceState==null)
            createFragments();
        else {}
    }
    private void createFragments()
    {
        FragmentConnection reguest= new Fragment_request();
        FragmentConnection table= new Fragment_table();
        reguest.setiFragmentConnetion(table.getiFragmentConnetion());
        fTrans=getFragmentManager().beginTransaction();
        fTrans.add(R.id.id_fragment_request,reguest);
        fTrans.add(R.id.id_fragment_table,table);
        fTrans.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
