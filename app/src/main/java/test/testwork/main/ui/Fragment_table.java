package test.testwork.main.ui;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import test.testwork.R;
import test.testwork.main.tools.DBHelper;
import test.testwork.main.tools.Table;

/**
 * Created by Gerbasha on 05.07.2014.
 */
public class Fragment_table extends FragmentConnection {
    Table table;

    public Fragment_table() {
        initializeConnectionMayConnection();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tabel, null);
        initializeTable(v);
        return v;

    }

    private void initializeConnectionMayConnection() {
        iFragmentConnectionMay = new IFragmentConnection() {
            @Override
            public void doRequest() {
                reloadTable();
            }
        };
    }

    private void initializeTable(View view) {
        LinearLayout ll = (LinearLayout) view.findViewById(R.id.id_fragment_table_linerLayout);
        table = new Table(view.getContext(), "Word", "Value");
        ll.addView(table);
    }

    @Override
    public void onResume() {
        super.onResume();
        fillTable(getActivity().getApplicationContext());
    }

    private void fillTable(Context context) {
        DBHelper dbHelper = new DBHelper(context);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor c = db.query("request", null, null, null, null, null, null);
        if (c.moveToFirst()) {
            int wordColIndex = c.getColumnIndex("word");
            int answerColIndex = c.getColumnIndex("answer");
            do {
                table.addRow(c.getString(wordColIndex), c.getString(answerColIndex));
            } while (c.moveToNext());

        }
    }

    public void reloadTable() {
        table.cleanTable();
        fillTable(getActivity().getApplicationContext());
    }

}
