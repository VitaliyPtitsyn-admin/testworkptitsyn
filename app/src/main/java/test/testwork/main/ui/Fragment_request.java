package test.testwork.main.ui;

import android.os.Bundle;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import test.testwork.R;
import test.testwork.main.reguset.ReguestGoogle;
import test.testwork.main.tools.DBHelper;
import test.testwork.main.tools.ICalBack;
import test.testwork.main.tools.Table;
import test.testwork.main.tools.TextAndNumbersInputFilter;

/**
 * Created by Gerbasha on 04.07.2014.
 */
public class Fragment_request extends FragmentConnection {
    Table table;
    Button find;
    ICalBack callBack;
    EditText ed;
    DBHelper dbHelper;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_request, null);
        ed = (EditText) v.findViewById(R.id.id_fragment_request_text);
        ed.setFilters(new InputFilter[]{new TextAndNumbersInputFilter()});
        initializeTable(v);
        initializeButton(v);
        initializeCallBack();
        dbHelper = new DBHelper(v.getContext());

        return v;
    }

    /**
        Инициализация функции поиска, активация при нажатии на кнопку.
     */
    public void initializeCallBack() {
        callBack = new ICalBack() {
            @Override
            public void setData(String... s) {
                table.cleanTable();
                String str1 = String.valueOf(ed.getText());
                String str2 = s[0].substring(0, 15);
                table.addRow(str1, str2);
                dbHelper.add(str1, str2);
                if(iFragmentConnectionFreendly!=null)
                    iFragmentConnectionFreendly.doRequest();
            }
        };
    }

    private void initializeTable(View view) {
        LinearLayout ll = (LinearLayout) view.findViewById(R.id.id_fragment_request_LinearLayout);
        table = new Table(view.getContext(), "Word", "Value");
        ll.addView(table);
    }

    private void initializeButton(View view) {

        find = (Button) view.findViewById(R.id.id_fragment_request_button_find);

        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReguestGoogle reguest = new ReguestGoogle();
                reguest.setCalBackLisener(callBack);
                String str1 = String.valueOf(ed.getText());
                if (str1.equals("")) {
                    str1 = "Light";
                    ed.setText(str1.toCharArray(), 0, str1.length());
                }
                reguest.execute(str1);
            }
        });

    }


}

