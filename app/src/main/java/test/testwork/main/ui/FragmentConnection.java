package test.testwork.main.ui;

import android.app.Fragment;

/**
 * Created by Gerbasha on 05.07.2014.
 */
public abstract  class FragmentConnection extends Fragment {
    protected IFragmentConnection iFragmentConnectionMay;
    protected IFragmentConnection iFragmentConnectionFreendly;

    public IFragmentConnection getiFragmentConnetion() {
        return iFragmentConnectionMay;
    }

    public void setiFragmentConnetion(IFragmentConnection iFragmentConnetion) {
        this.iFragmentConnectionFreendly = iFragmentConnetion;
    }
}
