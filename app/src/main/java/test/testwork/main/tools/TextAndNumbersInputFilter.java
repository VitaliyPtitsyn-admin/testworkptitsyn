package test.testwork.main.tools;

import android.text.InputFilter;
import android.text.Spanned;

import java.text.DecimalFormatSymbols;
import java.util.regex.Pattern;

/**
 * Created by Gerbasha on 06.07.2014.
 */
public class TextAndNumbersInputFilter implements InputFilter {

    @Override
    public CharSequence filter(CharSequence source, int start, int end,
                               Spanned dest, int dstart, int dend) {

        for (int i = start; i < end; i++) {
            if(source.charAt(i) >=48 && source.charAt(i) <=57  )
            {
                return null;
            }
            if(source.charAt(i) >=65 && source.charAt(i) <=90  )
            {
                return null;
            }
            if(source.charAt(i) >=97 && source.charAt(i) <=122  )
            {
                return null;
            }
            if(source.charAt(i) >=192 && source.charAt(i) <=233  )
            {
                return null;
            }
            if(source.charAt(i) >=224 && source.charAt(i) <=240  )
            {
                return null;
            }
        }
        return "";
    }
}
