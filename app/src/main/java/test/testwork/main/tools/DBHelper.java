package test.testwork.main.tools;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Gerbasha on 05.07.2014.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, "DB", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table request ("
                + "id integer primary key autoincrement,"
                + "word text,"
                + "answer text" + ");");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void add(String word,String answer)
    {
        ContentValues cv = new ContentValues();

        cv.put("word", word);
        cv.put("answer", answer);
        SQLiteDatabase db=getWritableDatabase();
        db.insert("request",null,cv);
    }

}
