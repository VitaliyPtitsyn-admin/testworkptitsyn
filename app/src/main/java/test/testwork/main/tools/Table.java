package test.testwork.main.tools;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * Created by Gerbasha on 04.07.2014.
 */
public class Table extends LinearLayout {

    String[] strings;
    TableLayout header;
    TableLayout table;
    private int widthColumn;
    private int columnCounter;

    public Table(Context context, String... strings) {
        super(context);

        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        setOrientation(VERTICAL);
//
        this.strings = strings;
        columnCounter = strings.length;
        initializationHeader();
        initializationTable();

    }

    private Table(Context context) {
        super(context);
    }

    private Table(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    private Table(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        widthColumn = getWidth() / columnCounter;
    }


    private void initializationHeader() {
        header = new TableLayout(getContext());
        header.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        header.setBackgroundColor(Color.rgb(204, 119, 34));

        TableRow tableRow = new TableRow(header.getContext());
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        tableRow.setLayoutParams(params);

        for (int i = 0; i < columnCounter; i++) {
            addStringToRow(tableRow, strings[i], Color.rgb(207, 181, 59));
        }
        header.addView(tableRow);
        addView(header);
    }

    private void initializationTable() {
        LayoutParams param = new LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        ScrollView scroll = new ScrollView(getContext());
//        param.weight=1f;
        scroll.setLayoutParams(param);
        table = new TableLayout(getContext());
//        table.setLayoutParams(param);
        table.setBackgroundColor(Color.rgb(204, 119, 34));
        for (int i = 0; i < columnCounter; i++) {
            table.setColumnStretchable(i, false);
        }
        scroll.addView(table);
        addView(scroll);
    }

    public void addRow(String... columns) {
        TableRow tableRow = new TableRow(getContext());
        ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        tableRow.setLayoutParams(params);
        for (int i = 0; i < columns.length; i++)
            addStringToRow(tableRow, columns[i], Color.rgb(107, 128, 0));
        table.addView(tableRow);
    }

    private void addStringToRow(TableRow tableRow, String string, int colorRGB) {
        TableRow.LayoutParams param = new TableRow.LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        param.setMargins(2, 0, 4, 2);
        param.weight = 1f;
        TextView col1 = new TextView(getContext());
        col1.setText(string);
        col1.setWidth(widthColumn);
        col1.setLayoutParams(param);
        col1.setTextColor(Color.rgb(245, 245, 220));
        col1.setBackgroundColor(colorRGB);
        tableRow.addView(col1);
    }


    public void cleanTable() {
        table.removeAllViews();
    }
}
